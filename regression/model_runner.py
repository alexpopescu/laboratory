import numpy as np

import statsmodels.formula.api as sm
from sklearn import metrics
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor

import common


class RegressionModelRunner(common.BaseModelRunner):
    def __init__(self, log):
        super().__init__(log)
        self.evaluations = []

    def backward_elimination(self, significance_level, columns):
        x = np.append(arr=self.x_train, values=self.x_test, axis=0)
        y = np.append(arr=self.y_train, values=self.y_test, axis=0)
        x_opt = np.append(arr=np.ones((x.shape[0], 1)).astype(int), values=x, axis=1)

        while True:
            regressor_ols = sm.OLS(endog=y, exog=x_opt).fit()
            p_values = regressor_ols.pvalues.astype(float)
            max_p_val = max(p_values)

            adj_r_before = regressor_ols.rsquared_adj.astype(float)
            if max_p_val <= significance_level:
                break

            j = np.where(p_values == max_p_val)[0][0]
            x_opt = np.delete(x_opt, j, axis=1)
            tmp_regressor = sm.OLS(endog=y, exog=x_opt).fit()
            adj_r_after = tmp_regressor.rsquared_adj.astype(float)

            if adj_r_before >= adj_r_after:
                break
            else:
                self.log.info('Removing column %s because it has a p value of %f', columns[j - 1], max_p_val)
                del columns[j - 1]
        return columns

    def run_all(self):
        self.evaluations.append(self.execute('Linear Regression', self.logistic_regression))
        self.evaluations.append(self.execute('RBF SVM', self.support_vector_machine))
        self.evaluations.append(self.execute('Decision Tree', self.decision_tree))
        self.evaluations.append(self.execute('Random Forest', self.random_forest))

    def execute(self, name, func, *args, **kwargs):
        with common.Timer(name, self._log):
            regressor = func(*args, **kwargs)
            regressor.fit(self.x_train, self.y_train)
            y_pred = regressor.predict(self.x_test)
            r2_score = metrics.r2_score(self.y_test, y_pred)
        self.log.info('Result for %s: R2 score = %f', name, r2_score)
        return r2_score

    @staticmethod
    def logistic_regression():
        return LinearRegression()

    @staticmethod
    def support_vector_machine():
        return SVR(kernel='rbf', gamma='auto')

    @staticmethod
    def decision_tree():
        return DecisionTreeRegressor(random_state=0)

    @staticmethod
    def random_forest():
        return RandomForestRegressor(n_estimators=10, random_state=0)
