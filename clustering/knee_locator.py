import numpy as np
from scipy import interpolate
from scipy.signal import argrelextrema


def knee_locator(x, y, sensitivity=1.0, curve='concave', direction='increasing'):
    """
    This function attempts to find the point of maximum curvature on a line
    Based on https://github.com/arvkevi/kneed
    :param x: x values.
    :type x: list or array.
    :param y: y values.
    :type y: list or array.
    :param sensitivity: Sensitivity, original paper suggests default of 1.0
    :type sensitivity: float
    :param curve: If 'concave', algorithm will detect knees. If 'convex', it
        will detect elbows.
    :type curve: string
    :param direction: one of {"increasing", "decreasing"}
    :type direction: string
    """
    # Step 0: Raw Input
    N = len(x)

    # Step 1: fit a smooth line
    uspline = interpolate.interp1d(x, y)
    Ds_x = np.linspace(np.min(x), np.max(x), N)
    Ds_y = uspline(Ds_x)

    # Step 2: normalize values
    xsn = (Ds_x - min(Ds_x)) / (max(Ds_x) - min(Ds_x))
    ysn = (Ds_y - min(Ds_y)) / (max(Ds_y) - min(Ds_y))

    # Step 3: Calculate difference curve
    xd = xsn
    if curve == 'convex' and direction == 'decreasing':
        yd = ysn + xsn
        yd = 1 - yd
    elif curve == 'concave' and direction == 'decreasing':
        yd = ysn + xsn
    elif curve == 'concave' and direction == 'increasing':
        yd = ysn - xsn
    if curve == 'convex' and direction == 'increasing':
        yd = abs(ysn - xsn)

    # Step 4: Identify local maxima/minima
    # local maxima
    xmx_idx = argrelextrema(yd, np.greater)[0]
    xmx = xd[xmx_idx]
    ymx = yd[xmx_idx]

    # local minima
    xmn_idx = argrelextrema(yd, np.less)[0]
    xmn = xd[xmn_idx]
    ymn = yd[xmn_idx]

    # Step 5: Calculate thresholds
    Tmx = ymx - (sensitivity * np.diff(xsn).mean())

    # Step 6: find knee
    if not xmx_idx.size:
        raise Exception("No local maxima found in the distance curve\n"
                        "The line is probably not polynomial, try plotting\n"
                        "the distance curve with plt.plot(knee.xd, knee.yd)\n"
                        "Also check that you aren't mistakenly setting the curve argument")

    mxmx_iter = np.arange(xmx_idx[0], len(xsn))
    xmx_idx_iter = np.append(xmx_idx, len(xsn))

    knee_, norm_knee_, knee_x = 0.0, 0.0, None
    for mxmx_i, mxmx in enumerate(xmx_idx_iter):
        # stopping criteria for exhasuting array
        if mxmx_i == len(xmx_idx_iter) - 1:
            break
        # indices between maxima/minima
        idxs = (mxmx_iter > xmx_idx_iter[mxmx_i]) * \
            (mxmx_iter < xmx_idx_iter[mxmx_i + 1])
        between_local_mx = mxmx_iter[np.where(idxs)]

        for j in between_local_mx:
            if j in xmn_idx:
                # reached a minima, x indices are unique
                # only need to check if j is a min
                if yd[j + 1] > yd[j]:
                    Tmx[mxmx_i] = 0
                    knee_x = None  # reset x where yd crossed Tmx
                elif yd[j + 1] <= yd[j]:
                    raise Exception("If this is a minima, how would you ever get here:")
            if yd[j] < Tmx[mxmx_i] or Tmx[mxmx_i] < 0:
                # declare a knee
                if not knee_x:
                    knee_x = j
                knee_ = x[xmx_idx[mxmx_i]]
                norm_knee_ = xsn[xmx_idx[mxmx_i]]
    return knee_, norm_knee_, knee_x
