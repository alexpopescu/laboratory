# Clustering Experiments

## Model setup
For the clustering experiments, we need to decide on the number of clusters to use.
This package contains an implementation of an automated "Elbow Method" estimator and an automated "Dendrogram" estimator for the number of clusters to use.
After computing the two, the system will use the greater one to cluster the data. 

## Iris
For this experiment I'm going to use this dataset:
https://www.kaggle.com/rtatman/iris-dataset-json-version

#### Usage
iris_classification [--data|-d] <data file>

where:

* **data**: The location of the JSON input data file

#### Data PreProcessing
The data was clean, so no special preprocessing to do, but it contains the labels for classifying the data, which were removed before running the clustering.

#### Results
The "Elbow Method" recommended 3 clusters to be used, while the "Dendrogram Method" decided the best number is 2.
This dataset also contains the labels for the data, so we can evaluate how good the generated clusters are at the end.
The script will use the larger of the two numbers (k=3), which is exactly the number of labels in the data.

Here's what the final clusters looked like:

Using K-Means Clustering:

| Cluster | Dominating Label | Precision |
|---|---|---|
| 1 | versicolor | 77.4% |
| 2 | setosa | 100% |
| 3 | virginica | 94.7% |

Using Hierarchical Clustering

| Cluster | Dominating Label | Precision |
|---|---|---|
| 1 | versicolor | 76.6% |
| 2 | setosa | 100% |
| 3 | virginica | 97.2% |


## Customer Segmentation
For this experiment I'm going to use this dataset:
https://www.kaggle.com/vjchoudhary7/customer-segmentation-tutorial-in-python

#### Usage
customer_segmentation [--data|-d] <data file>

where:

* **data**: The location of the CSV input data file

#### Data PreProcessing
The data was clean, so no special preprocessing to do, but the frist two columns were eliminated as irrelevant (customer ID and gender) and the ones kept for clustering were just: age, income and spending score.

#### Results
The "Elbow Method" recommended 4 clusters to be used, while the "Dendrogram Method" decided the best number is 3.
The script will use the larger of the two numbers (k=4).
Here's what the final clusters looked like, including the distribution (mean and standard deviation) for all 3 variables:

Using K-Means Clustering:

| Cluster | Age | Annual Income (k$) | Spending Score (1-100) |
|---|---|---|---|
| 1 | μ=40.39, σ=11.23 | μ=87.00, σ=16.06 | μ=18.63, σ=10.77 |
| 2 | μ=32.69, σ=3.68 | μ=86.54, σ=16.10 | μ=82.13, σ=9.24 |
| 3 | μ=44.89, σ=15.26 | μ=48.71, σ=14.45 | μ=42.63, σ=14.64 |
| 4 | μ=24.82, σ=5.47 | μ=28.71, σ=10.20 | μ=74.25, σ=13.82 |

Using Hierarchical Clustering

| Cluster | Age | Annual Income (k$) | Spending Score (1-100) |
|---|---|---|---|
| 1 | μ=42.82, σ=15.79 | μ=48.58, σ=14.85 | μ=43.51, σ=14.64 |
| 2 | μ=41.69, σ=10.74 | μ=88.23, σ=16.13 | μ=17.29, σ=10.06 |
| 3 | μ=32.69, σ=3.68 | μ=86.54, σ=16.10 | μ=82.13, σ=9.24 |
| 4 | μ=24.85, σ=4.90 | μ=24.95, σ=7.10 | μ=81.00, σ=9.27 |

#### Analysis
It looks like the Age column does not influence the results a lot and the groups are mainly formed around the income and spending levels.
And, although the clusters change order between the two models, we can clearly see that they are very similar:

* average income, average spending
* high income, low spending
* high income, high spending
* low income, high spending

When eliminating the "Age" feature, 5 clusters will be created and a new group (low income, low spending) appears, which in the previous model was probably incorporated by the average income and average spending cluster, where we can observe a quite high standard deviation for both variables.

