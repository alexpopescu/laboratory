from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

import common
from text_classification.confusion_matrix import ConfusionMatrix


class ClassificationModelRunner(common.BaseModelRunner):
    def __init__(self, log):
        super().__init__(log)
        self.confusion_matrices = []

    def run_all(self):
        self.confusion_matrices.append(self.execute('Logistic Regression', self.logistic_regression))
        self.confusion_matrices.append(self.execute('K-Nearest Neighbors', self.k_nearest_neighbors))
        self.confusion_matrices.append(self.execute('Linear SVM', self.support_vector_machine, kernel='linear'))
        self.confusion_matrices.append(self.execute('RBF SVM', self.support_vector_machine, kernel='rbf'))
        self.confusion_matrices.append(self.execute('Naive Bayes', self.naive_bayes))
        self.confusion_matrices.append(self.execute('Decision Tree', self.decision_tree))
        self.confusion_matrices.append(self.execute('Random Forest', self.random_forest))

    def execute(self, name, func, *args, **kwargs):
        with common.Timer(name, self._log):
            classifier = func(*args, **kwargs)
            classifier.fit(self.x_train, self.y_train)
            y_pred = classifier.predict(self.x_test)
            cm = ConfusionMatrix(name, confusion_matrix(y_true=self.y_test, y_pred=y_pred))
            self.log.info(cm)
        return cm

    @staticmethod
    def logistic_regression():
        return LogisticRegression(random_state=0, solver='liblinear', multi_class='ovr')

    @staticmethod
    def k_nearest_neighbors():
        return KNeighborsClassifier(n_neighbors=5, metric='minkowski', p=2)

    @staticmethod
    def support_vector_machine(kernel='linear'):
        return SVC(kernel=kernel, gamma='auto', random_state=0)

    @staticmethod
    def naive_bayes():
        return GaussianNB()

    @staticmethod
    def decision_tree():
        return DecisionTreeClassifier(criterion='entropy', random_state=0)

    @staticmethod
    def random_forest():
        return RandomForestClassifier(n_estimators=10, criterion='entropy', random_state=0)
