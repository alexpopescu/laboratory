# Text Classification Experiments

## Sarcasm Detection in Article Titles
For this experiment I'm going to use this dataset:
https://www.kaggle.com/rmisra/news-headlines-dataset-for-sarcasm-detection

#### Usage
sarcasm_detector [--data|-d] <data file> [--max-features|-m] <num>

where:

* **data**: The location of the JSON input data file
* **max-features**: The maximum number of words to consider (top) 

#### Data PreProcessing
The data is expected to be in a JSON lines format with one record per line with the following columns: article_link, headline, is_sarcastic.

The headlines were cleaned up using the following steps:

* any non-letter character was removed
* stopwords were removed using the stopword list from NLTK
* remaining words were stemmed using the PorterStemmer from NLTK
* when running the model, only the 1500 (configurable) top words were included

In total, the dataset contains 26709 entries, of which 11724 are flagged as "sarcastic".
The dataset was split into a training and a test set with a ratio of 80/20

#### Results
| Model | Accuracy | Precision | Recall | F1 Score | Time (s) |
|---|---|---|---|---|---|
|**Logistic Regression**|0.755|0.753|0.654|0.700|1|
|**K-Nearest Neighbors**|0.638|0.602|0.502|0.548|317|
|**Linear SVM**|0.753|0.756|0.644|0.695|1215|
|**RBF SVM**|0.598|0.895|0.091|0.166|1078|
|**Naive Bayes**|0.694|0.615|0.802|0.696|2.5|
|**Decision Tree**|0.685|0.642|0.634|0.638|232|
|**Random Forest**|0.718|0.700|0.622|0.659|42|


## Spam Classification
For this experiment I'm going to use this dataset:
https://www.kaggle.com/veleon/ham-and-spam-dataset

#### Usage
spam_classifier [--data|-d] <data dir> [--max-features|-m] <num>

where:

* **data**: The directory containing the data
* **max-features**: The maximum number of words to consider (top) 

#### Data PreProcessing
The dataset is expected contain email messages divided into two subdirectories, "ham" and "spam".

The messages were cleaned up using the following steps:

* everything before the first empty line was discarded in order to avoid including the message headers
* any non-letter character was removed
* stopwords were removed using the stopword list from NLTK
* remaining words were stemmed using the PorterStemmer from NLTK
* when running the model, only the 1500 (configurable) top words were included

In total, the dataset contains 3052 entries, 501 flagged as spam and 2551 flagged as ham

#### Results
| Model | Accuracy | Precision | Recall | F1 Score | Time (s) |
|---|---|---|---|---|---|
|**Logistic Regression**|0.987|0.979|0.940|0.959|0.256|
|**K-Nearest Neighbors**|0.943|0.922|0.710|0.802|4.780|
|**Linear SVM**|0.979|0.939|0.930|0.935|1.705|
|**RBF SVM**|0.953|0.949|0.750|0.838|4.796|
|**Naive Bayes**|0.969|0.893|0.920|0.906|0.242|
|**Decision Tree**|0.966|0.891|0.900|0.896|1.278|
|**Random Forest**|0.974|0.967|0.870|0.916|0.287|


## Sentiment Analysis of Movie Reviews
For this experiment I'm going to use this dataset:
https://www.kaggle.com/iarunava/imdb-movie-reviews-dataset

#### Usage
sentiment_analysis [--data|-d] <data dir> [--max-features|-m] <num>

where:

* **data**: The directory containing the data
* **max-features**: The maximum number of words to consider (top) 

#### Data PreProcessing
The dataset is expected contain reviews (one per file) already divided into "train" and "test" directories.
Then each set is divided into "neg" (negative) and "pos" (positive) subdirectories. 

The number of included reviews for each combination is:

| | negative | positive |
|---|---|---|
| **training** | 12500 | 12500 |
| **testing** | 12500 | 12500 |

In total, the dataset contains 50000

The messages were cleaned up using the following steps:

* any non-letter character was removed
* stopwords were removed using the stopword list from NLTK
* remaining words were stemmed using the PorterStemmer from NLTK
* when running the model, only the 1500 (configurable) top words were included

#### Results
| Model | Accuracy | Precision | Recall | F1 Score | Time (s) |
|---|---|---|---|---|---|
|**Logistic Regression**|0.861|0.858|0.864|0.861|4|
|**K-Nearest Neighbors**|0.619|0.621|0.614|0.617|1558|
|**Linear SVM**|0.859|0.857|0.864|0.860|9309|
|**RBF SVM**|0.861|0.846|0.882|0.864|1468|
|**Naive Bayes**|0.737|0.819|0.608|0.698|4|
|**Decision Tree**|0.713|0.715|0.708|0.712|31|
|**Random Forest**|0.779|0.819|0.716|0.764|7|

