class ConfusionMatrix:
    def __init__(self, name, conf_matrix):
        self.name = name
        self.tn = conf_matrix[0, 0]
        self.fn = conf_matrix[1, 0]
        self.fp = conf_matrix[0, 1]
        self.tp = conf_matrix[1, 1]

    @property
    def accuracy(self):
        return (self.tn + self.tp) / (self.tn + self.fn + self.fp + self.tp)

    @property
    def precision(self):
        return self.tp / (self.tp + self.fp)

    @property
    def recall(self):
        return self.tp / (self.tp + self.fn)

    @property
    def f1_score(self):
        return 2 * self.precision * self.recall / (self.precision + self.recall)

    def __repr__(self):
        return '{}(accuracy={:.3f}, precision={:.3f}, recall={:.3f}, f1_score={:.3f})'.format(
            self.name, self.accuracy, self.precision, self.recall, self.f1_score
        )
