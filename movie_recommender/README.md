# Movie Recommender

For this experiment I'm going to use the MovieLens dataset:
http://grouplens.org/datasets/movielens/

#### Usage
recommend [--data|-d] <data path> [--user|-u] <id> [--min-movie-ratings|-r] <num> [--max-user-ratings|-x] <num> [--num-recommendations] <num>

where:

* **data**: The location of the input data files
* **user**: The ID of the user to give recommendations to
* **min-movie-ratings**: The minimum amount of ratings needed for a movie to be taken into account (default: 100)
* **max-user-ratings**: The maximum amount of ratings needed for a user to be taken into account (default: 1000)
* **num-recommendations**: The number of movie recommendations to return (default: 20)

#### Steps

* First remove users who have given too many ratings and movies that have received too few of them (see parameters above).
* Calculate item-based correlations between the movies rated by the given user and all other movies.
* The correlation is calculated using the Pearson Correlation Coefficient:
![\rho{X,Y}={\frac {\operatorname{cov}(X,Y)}{\sigma{X}\sigma{Y}}}](https://latex.codecogs.com/png.latex?\rho{X,Y}={\frac{\operatorname{cov}%28X,Y%29}{\sigma{X}\sigma{Y}}})
* For each movie correlated to a movie rated by the given user, the score will be the correlation coefficient multiplied by the user rating
* The list is sorted by score descendingly and the top movies are returned 

In total, the dataset contains 58,098 movies, 283,228 users and 27,753,444 ratings

When pruning movies based on a minimum of 100 ratings, 47,598 of them will be removed

#### Results
For an example user with the following ratings:

| Title | Rating |
|---|---|
| Diabolique (1996) | 3.0 |
| Adventures of Pinocchio, The (1996) | 4.0 |
| Angel on My Shoulder (1946) | 3.0 |
| Godfather: Part II, The (1974) | 4.0 |
| American Werewolf in London, An (1981) | 4.0 |
| The Devil's Advocate (1997) | 4.0 |
| The Players Club (1998) | 3.0 |
| Halloween 4: The Return of Michael Myers (1988) | 2.0 |
| Rapture, The (1991) | 3.0 |
| Saving Private Ryan (1998) | 5.0 |
| Room at the Top (1959) | 4.0 |

The recommended movies will be:

| Title | Score |
|---|---|
| Godfather, The (1972) | 103705 |
| Forrest Gump (1994) | 66286 |
| Schindler's List (1993) | 63893 |
| Braveheart (1995) | 63226 |
| Gladiator (2000) | 56530 |
| Shawshank Redemption, The (1994) | 56302 |
| Matrix, The (1999) | 46139 |
| Jurassic Park (1993) | 44919 |
| Silence of the Lambs, The (1991) | 44636 |
| Raiders of the Lost Ark (1981) | 43724 |
| Sixth Sense, The (1999) | 42591 |
| Star Wars: Episode IV - A New Hope (1977) | 41207 |
| Apollo 13 (1995) | 40651 |
| Good Will Hunting (1997) | 40535 |
| Star Wars: Episode V - The Empire Strikes Back (1980) | 36715 |
| Lord of the Rings: The Fellowship of the Ring, The (2001) | 36495 |
| Lord of the Rings: The Two Towers, The (2002) | 36173 |
| Terminator 2: Judgment Day (1991) | 36020 |
| Toy Story (1995) | 35191 |
| Green Mile, The (1999) | 35049 |

