from keras.callbacks import LambdaCallback


def create_keras_callback(log):
    def epoch_log(epoch, logs):
        log.info('Epoch {}: loss={:.4f}, accuracy={:.4f}, val loss={:.4f}, val accuracy={:.4f}'.format(
            epoch, logs['loss'], logs['acc'], logs['val_loss'], logs['val_acc']))

    def batch_log(batch, logs):
        log.debug('Batch {}: loss={:.4f}, accuracy={:.4f}'.format(batch, logs['loss'], logs['acc']))

    return LambdaCallback(on_epoch_end=epoch_log,
                          on_batch_end=batch_log)
