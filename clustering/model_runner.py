from scipy.cluster import hierarchy
from sklearn.cluster import KMeans, AgglomerativeClustering
import numpy as np
import common
from clustering.knee_locator import knee_locator


class ClusteringModelRunner:
    def __init__(self, log):
        self.log = log
        self.x = None
        self.columns = None
        self.y = None
        self.k = None

    def load_dataset(self, dataset, columns, labels=None):
        self.x = dataset
        self.columns = columns
        self.y = labels

    def _log(self, text):
        self.log.info(text)

    def use_elbow_method(self):
        wcss = []
        for i in range(1, 11):
            model = KMeans(n_clusters=i, init='k-means++', max_iter=300, n_init=10, random_state=0)
            model.fit(self.x)
            wcss.append(model.inertia_)

        k, _, _ = knee_locator(range(1, 11), wcss, curve='convex', direction='decreasing')
        return k

    def use_dendrogram(self):
        link_matrix = hierarchy.linkage(self.x, method='ward')
        linkage = link_matrix[:, 2]
        count = 2
        clusters = 0
        max_diff = 0
        for i in range(len(linkage)-1, 0, -1):
            diff = linkage[i] - linkage[i-1]
            if diff > max_diff:
                max_diff = diff
                clusters = count
            count += 1
        return clusters

    def run_all(self):
        self.execute('K-Means Clustering', self.k_means_clustering, k=self.k)
        self.execute('Hierarchical Clustering', self.hierarchical_clustering, k=self.k)

    def execute(self, name, func, *args, **kwargs):
        if self.k is None:
            self.k = self.use_elbow_method()
        with common.Timer(name, callback=self._log):
            model = func(*args, **kwargs)
            clusters = model.fit_predict(self.x)

            if self.y is None:
                result = self.__describe_clusters(clusters, self.x, self.columns)
                for i in range(self.k):
                    description = '; '.join(
                        '{}: mean={:.2f}, std={:.2f}'.format(k, v[0], v[1]) for k, v in result[i].items())
                    self.log.info('Cluster %d: %s', i, description)
            else:
                evaluation, total_precision = self.__evaluate_clusters(clusters, self.y)
                for i in range(self.k):
                    self.log.info('Cluster %d: best label=%s, precision=%f', i, evaluation[i][0], evaluation[i][1])
                self.log.info('Total precision: %f', total_precision)

    @staticmethod
    def k_means_clustering(k):
        return KMeans(n_clusters=k, init='k-means++', max_iter=300, n_init=10, random_state=0)

    @staticmethod
    def hierarchical_clustering(k):
        return AgglomerativeClustering(n_clusters=k, affinity='euclidean', linkage='ward')

    def __evaluate_clusters(self, clusters, labels):
        combined = list(zip(labels, clusters))
        cluster_labels = []
        for i in range(self.k):
            cluster_labels.append([label for label, cluster in combined if cluster == i])

        result = []
        total_correct = 0
        for i in range(self.k):
            unique, counts = np.unique(cluster_labels[i], return_counts=True)
            max_idx = np.argmax(counts)
            precision = counts[max_idx] / sum(counts)
            result.append((unique[max_idx], precision))
            total_correct += counts[max_idx]
        return result, total_correct / len(labels)

    def __describe_clusters(self, clusters, data, columns):
        ret = []
        for i in range(self.k):
            cluster_data = data[np.isin(clusters, [i])]
            result = {}
            for j in range(len(columns)):
                column_data = cluster_data[:, j]
                result[columns[j]] = (column_data.mean(), column_data.std())
            ret.append(result)
        return ret
