import argparse
import io
import os
import time

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer

import common
from text_classification import util
from text_classification.model_runner import ClassificationModelRunner


def parse_arguments():
    parser = argparse.ArgumentParser(description='Classifies sentiment in text as positive or negative')
    parser.add_argument('--data', '-d', dest='data',
                        help='The dataset directory (expects "test" and "train" each with "neg" and "pos")')
    parser.add_argument('--max-features', '-m', dest='max_features', default=None, type=int,
                        help='The maximum number of features to consider (top)')
    return parser.parse_args()


def read_files(path):
    for root, _, filenames in os.walk(path):
        for filename in filenames:
            path = os.path.join(root, filename)
            with io.open(path, 'r', encoding='latin1') as f:
                message = f.read()
                yield path, message


def data_frame_from_directory(path, classification):
    rows = []
    index = []
    for filename, message in read_files(path):
        rows.append({'review': message, 'class': classification})
        index.append(filename)

    return pd.DataFrame(rows, index=index)


def main():
    args = parse_arguments()
    log = common.create_logger()
    start_time = time.time()

    with common.Timer('Loading dataset from {}'.format(args.data), callback=log.info):
        train_dataset = data_frame_from_directory(os.path.join(args.data, 'train', 'neg'), 'neg')
        train_dataset = train_dataset.append(
            data_frame_from_directory(os.path.join(args.data, 'train', 'pos'), 'pos'), sort=False)
        test_dataset = data_frame_from_directory(os.path.join(args.data, 'test', 'neg'), 'neg')
        test_dataset = test_dataset.append(
            data_frame_from_directory(os.path.join(args.data, 'test', 'pos'), 'pos'), sort=False)

    with common.Timer('Data PreProcessing', callback=log.info):
        n = train_dataset.shape[0]
        train_reviews = [util.text_clean_up(train_dataset['review'][i]) for i in range(0, n)]
        test_reviews = [util.text_clean_up(test_dataset['review'][i]) for i in range(0, n)]

        cv = CountVectorizer(max_features=args.max_features)
        x_train = cv.fit_transform(train_reviews).toarray()
        x_test = cv.transform(test_reviews).toarray()
        y_train = train_dataset['class']
        y_test = test_dataset['class']

    log.info('Found {} entries'.format(x_train.shape[0] + x_test.shape[0]))
    log.info('Using {} features (words)'.format(args.max_features or 'ALL'))

    model_runner = ClassificationModelRunner(log)
    model_runner.load_train_test_dataset(x_train, y_train, x_test, y_test)
    model_runner.run_all()

    elapsed_time = time.time() - start_time
    log.info('The job took {:.4f} seconds to run'.format(elapsed_time))
    log.info('All done!')


if __name__ == "__main__":
    main()
