import re
import nltk


def text_clean_up(review, remove_stopwords=True, do_stemming=True):
    # remove special characters and numbers
    review = re.sub('[^a-zA-Z]', ' ', review)
    review = review.lower()

    if remove_stopwords or do_stemming:
        # split text and remove stopwords
        # Note: download the stopwords (only needed the first time)
        # nltk.download('stopwords')
        stopwords = set(nltk.corpus.stopwords.words('english')) if remove_stopwords else set()
        if do_stemming:
            ps = nltk.PorterStemmer()
            review = [ps.stem(w) for w in review.split() if w not in stopwords]
        else:
            review = [w for w in review.split() if w not in stopwords]

    return ' '.join(review)
