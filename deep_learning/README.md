# Text Classification Experiments

## Image Classification
For this experiment I'm going to use this dataset:
https://www.kaggle.com/prasunroy/natural-images

#### Usage
image_classification [--data|-d] <data dir> [--test-size|-t] <float> [--epochs|-e] <num>

where:

* **data**: The location of the image data set
* **test-size**: The percentage of data to be used for testing (given as a number between 0 and 1) 
* **epochs**: The number of epochs to use for the training

#### Data PreProcessing
First, the image names were loaded from the class folders into a *DataFrame*.
Then I've used a Keras *ImageDataGenerator* to augment the data set with sheared, zoomed or flipped images.
The dataset was split into a training and a test set (proportionally 80%-20%).

#### Modeling
The Convolutional Net will have an input layer of size 64x64x3 because the images will be resized to 64x64 and they have 3 channels.
I've added two 3x3 convolutional layers, each followed by a 2x2 max pooling layer. 
Each convolutional layer will apply a 3x3 kernel activated by a rectified linear unit (ReLU).
After the convolutional layers, I've added two hidden layers (with 128 nodes) and the output layer.
The output will have 8 nodes, as many as the image categories in the data (airplane, car, cat, dog, flower, fruit, motorbike, person).

#### Results
With the above setup it looks like the Validation accuracy plateaus around 90-91%

| Epoch | Loss | Accuracy | Validation Loss | Validation Accuracy |
|---:|---:|---:|---:|---:|
|  5 |0.2688|0.8956|0.2993|0.8775|
| 10 |0.1765|0.9344|0.2690|0.8993|
| 15 |0.1380|0.9484|0.2469|0.9138|
| 20 |0.0941|0.9652|0.2800|0.9058|
| 25 |0.0789|0.9716|0.3218|0.9014|
| 30 |0.0603|0.9786|0.3046|0.9145|
| 35 |0.0446|0.9844|0.3648|0.9051|
| 40 |0.0496|0.9822|0.3818|0.9065|
| 45 |0.0418|0.9862|0.3386|0.9080|
| 50 |0.0302|0.9893|0.4142|0.9007|

So, then I started trying out different things. First adding learning rate decay, 
to check if the system is stuck in some local minimum, but the results were not much better.

| Epoch | Loss | Accuracy | Validation Loss | Validation Accuracy |
|---:|---:|---:|---:|---:|
|  5 |0.2600|0.8996|0.3586|0.8630|
| 10 |0.1705|0.9395|0.2946|0.8986|
| 15 |0.1034|0.9618|0.2487|0.9217|
| 20 |0.0911|0.9663|0.3208|0.9014|
| 25 |0.0572|0.9803|0.2777|0.9123|
| 30 |0.0495|0.9824|0.3215|0.9196|
| 35 |0.0356|0.9889|0.3281|0.9196|
| 40 |0.0392|0.9866|0.3206|0.9138|
| 45 |0.0305|0.9897|0.3953|0.9101|
| 50 |0.0113|0.9973|0.3846|0.9051|

The I tried with 3 hidden layers instead of 2. 
This version was able to store more information, so the training accuracy went up faster 
and the validation accuracy also improved a bit, while training took only 2-3% longer.

| Epoch | Loss | Accuracy | Validation Loss | Validation Accuracy |
|---:|---:|---:|---:|---:|
|  5 |0.2762|0.8984|0.3964|0.8514|
| 10 |0.1656|0.9411|0.2339|0.9167|
| 15 |0.1227|0.9571|0.2620|0.9080|
| 20 |0.0866|0.9683|0.2941|0.9145|
| 25 |0.0561|0.9788|0.2737|0.9217|
| 30 |0.0555|0.9797|0.3049|0.9217|
| 35 |0.0303|0.9899|0.3634|0.9072|
| 40 |0.0315|0.9871|0.2763|0.9239|
| 45 |0.0274|0.9891|0.3262|0.9225|
| 50 |0.0271|0.9902|0.3622|0.9167|

And then I tried keeping the 2 hidden layers but adding a DropOut (with 0.2 rate) for both hidden layers.
This version equalized the training accuracy and validation accuracy a bit more, as expected, 
so it made the training accuracy raise slower while pushing the validation accuracy higher, but even after 100 epochs it was still fluctuating around 92-93%

| Epoch | Loss | Accuracy | Validation Loss | Validation Accuracy |
|---:|---:|---:|---:|---:|
|   5 |0.3203|0.8810|0.2955|0.8833|
|  10 |0.2058|0.9224|0.2768|0.9058|
|  15 |0.1601|0.9391|0.2967|0.9087|
|  20 |0.1459|0.9455|0.2166|0.9145|
|  25 |0.1028|0.9623|0.2508|0.9145|
|  30 |0.0962|0.9641|0.2547|0.9217|
|  35 |0.0788|0.9697|0.2328|0.9246|
|  40 |0.0712|0.9730|0.2738|0.9217|
|  45 |0.0576|0.9812|0.2773|0.9196|
|  50 |0.0565|0.9797|0.2792|0.9203|
|  55 |0.0539|0.9815|0.2655|0.9254|
|  60 |0.0435|0.9839|0.2580|0.9268|
|  65 |0.0375|0.9875|0.2889|0.9261|
|  70 |0.0376|0.9871|0.2980|0.9254|
|  75 |0.0309|0.9893|0.3004|0.9290|
|  80 |0.0295|0.9877|0.3547|0.9188|
|  85 |0.0294|0.9900|0.3172|0.9196|
|  90 |0.0217|0.9913|0.3605|0.9246|
|  95 |0.0231|0.9920|0.2958|0.9297|
| 100 |0.0241|0.9917|0.3314|0.9275|

In a last experiment I used images resized to 128x128 and 4 hidden layers with 0.2 DropOut.
It looks a bit better but the final validation accuracy only slightly improves over the previous experiments:

| Epoch | Loss | Accuracy | Validation Loss | Validation Accuracy |
|---:|---:|---:|---:|---:|
|   5 |0.3952|0.8433|0.3835|0.8493|
|  10 |0.2940|0.8884|0.3035|0.8754|
|  15 |0.2419|0.9065|0.2778|0.8986|
|  20 |0.2035|0.9246|0.2449|0.9036|
|  25 |0.1777|0.9431|0.2406|0.9130|
|  30 |0.1508|0.9476|0.2729|0.9072|
|  35 |0.1461|0.9500|0.2534|0.9094|
|  40 |0.1459|0.9518|0.2714|0.9196|
|  45 |0.1207|0.9614|0.3003|0.9152|
|  50 |0.1277|0.9583|0.2893|0.9123|
|  55 |0.1085|0.9647|0.2753|0.9210|
|  60 |0.1559|0.9498|0.2575|0.9283|
|  65 |0.1084|0.9663|0.2401|0.9217|
|  70 |0.0960|0.9706|0.2753|0.9159|
|  75 |0.0888|0.9716|0.2676|0.9239|
|  80 |0.0857|0.9725|0.2209|0.9232|
|  85 |0.0924|0.9697|0.3013|0.9246|
|  90 |0.0734|0.9768|0.2687|0.9246|
|  95 |0.0890|0.9728|0.3108|0.9167|
| 100 |0.0631|0.9810|0.2319|0.9319|


## Pneumonia Diagnose
For this experiment I'm going to use this dataset:
https://www.kaggle.com/paultimothymooney/chest-xray-pneumonia

#### Usage
pneumonia_diagnose [--data|-d] <data dir> [--epochs|-e] <num>

where:

* **data**: The location of the image data set
* **epochs**: The number of epochs to use for the training

#### Data PreProcessing
This dataset is conveniently split into training and test datasets, with approximately 10% of the data being test data, so we don't need to do that ourselves.
First, the image names were loaded from the class folders into two *DataFrames*, a training one and a test one.
Then I've used a Keras *ImageDataGenerator* to augment the data set with sheared, zoomed or flipped images.

#### Modeling
The Convolutional Net will have an input layer of size 64x64x3 because the images will be resized to 64x64 and they have 3 channels.
I've added two 3x3 convolutional layers, each followed by a 2x2 max pooling layer. 
Each convolutional layer will apply a 3x3 kernel activated by a rectified linear unit (ReLU).
After the convolutional layers, I've added two hidden layers (with 128 nodes) and the output layer.
The output will have 1 nodes, as this is a binary classification problem.

#### Results
These are the results after training the network for 100 epochs wi two input layers and no DropOut

| Epoch | Loss | Accuracy | Validation Loss | Validation Accuracy |
|---:|---:|---:|---:|---:|
|   5 |0.3122|0.8620|0.3714|0.8446|
|  10 |0.2562|0.8919|0.3559|0.8333|
|  15 |0.2236|0.9091|0.3386|0.8686|
|  20 |0.2074|0.9153|0.3631|0.8622|
|  25 |0.1780|0.9294|0.3291|0.8814|
|  30 |0.1696|0.9327|0.4564|0.8494|
|  35 |0.1519|0.9454|0.2503|0.9038|
|  40 |0.1421|0.9446|0.3659|0.8798|
|  45 |0.1451|0.9480|0.2775|0.9087|
|  50 |0.1349|0.9465|0.3410|0.8798|
|  55 |0.1264|0.9526|0.3119|0.8846|
|  60 |0.1207|0.9546|0.3692|0.8846|
|  65 |0.1125|0.9580|0.2215|0.9311|
|  70 |0.1166|0.9540|0.2606|0.9119|
|  75 |0.1098|0.9569|0.2689|0.9071|
|  80 |0.1084|0.9569|0.3093|0.9038|
|  85 |0.1068|0.9590|0.2372|0.9231|
|  90 |0.0993|0.9651|0.2958|0.9006|
|  95 |0.0994|0.9638|0.2852|0.9054|
| 100 |0.0973|0.9601|0.3681|0.8782|

And these are the results after 100 epochs, using 4 hidden layers and DropOut of 0.2 after each one

| Epoch | Loss | Accuracy | Validation Loss | Validation Accuracy |
|---:|---:|---:|---:|---:|
|   5 |0.3926|0.8236|0.3860|0.8237|
|  10 |0.2781|0.8892|0.4769|0.8173|
|  15 |0.2391|0.9028|0.4277|0.8574|
|  20 |0.2051|0.9178|0.4135|0.8590|
|  25 |0.1897|0.9291|0.3002|0.8926|
|  30 |0.1808|0.9342|0.2818|0.8942|
|  35 |0.1609|0.9375|0.3353|0.8846|
|  40 |0.1499|0.9448|0.2841|0.8990|
|  45 |0.1469|0.9465|0.3799|0.8862|
|  50 |0.1436|0.9473|0.2879|0.8990|
|  55 |0.1307|0.9536|0.3156|0.8942|
|  60 |0.1299|0.9546|0.3053|0.8942|
|  65 |0.1256|0.9559|0.3720|0.9038|
|  70 |0.1176|0.9582|0.3820|0.8974|
|  75 |0.1111|0.9601|0.3291|0.9183|
|  80 |0.1133|0.9574|0.2922|0.9183|
|  85 |0.1044|0.9626|0.2473|0.9231|
|  90 |0.1036|0.9628|0.3428|0.8990|
|  95 |0.0979|0.9661|0.2754|0.9215|
| 100 |0.1015|0.9638|0.3010|0.9038|

