import argparse
import time

import pandas as pd

import common
from regression.model_runner import RegressionModelRunner


def parse_arguments():
    parser = argparse.ArgumentParser(description='Regression of wine quality based on various predictive variables')
    parser.add_argument('--data', '-d', dest='data', help='The location of the CSV input data file')
    parser.add_argument('--test-size', '-t', dest='test_size', default=0.2, type=float,
                        help='The percentage of data to be used for testing (given as a number between 0 and 1)')
    return parser.parse_args()


def main():
    args = parse_arguments()
    log = common.create_logger()
    start_time = time.time()

    with common.Timer('Loading dataset from {}'.format(args.data), callback=log.info):
        dataset = pd.read_csv(args.data)

    with common.Timer('Automatic Backward Elimination', callback=log.info):
        x = dataset.iloc[:, :-1].values
        y = dataset.iloc[:, -1].values

        model_runner = RegressionModelRunner(log)
        model_runner.load_single_dataset(x, y, test_size=args.test_size)
        model_runner.normalize_data()
        columns = model_runner.backward_elimination(significance_level=0.05, columns=dataset.columns.values.tolist())
        log.info('Columns remaining after Backward Elimination: %s', ', '.join(columns))

    log.info('Executing models')
    x = pd.concat([dataset.iloc[:, 1:3], dataset.iloc[:, 4:7], dataset.iloc[:, 8:-1]], axis=1).values
    y = dataset.iloc[:, -1].values
    model_runner = RegressionModelRunner(log)
    model_runner.load_single_dataset(x, y, test_size=args.test_size)
    model_runner.normalize_data()
    model_runner.run_all()

    elapsed_time = time.time() - start_time
    log.info('The job took {:.4f} seconds to run'.format(elapsed_time))
    log.info('All done!')


if __name__ == "__main__":
    main()
