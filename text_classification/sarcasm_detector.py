import argparse
import time

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer

import common
from text_classification import util
from text_classification.model_runner import ClassificationModelRunner


def parse_arguments():
    parser = argparse.ArgumentParser(description='Detects sarcasm in headlines')
    parser.add_argument('--data', '-d', dest='data', help='The location of the JSON input data file')
    parser.add_argument('--max-features', '-m', dest='max_features', default=None, type=int,
                        help='The maximum number of words to consider (top)')
    parser.add_argument('--test-size', '-t', dest='test_size', default=0.2, type=float,
                        help='The percentage of data to be used for testing (given as a number between 0 and 1)')
    return parser.parse_args()


def main():
    args = parse_arguments()
    log = common.create_logger()
    start_time = time.time()

    with common.Timer('Loading dataset from {}'.format(args.data), callback=log.info):
        dataset = pd.read_json(args.data, lines=True)

    with common.Timer('Data PreProcessing', callback=log.info):
        n = dataset.shape[0]
        headlines = [util.text_clean_up(dataset['headline'][i]) for i in range(0, n)]

        cv = CountVectorizer(max_features=args.max_features)
        x = cv.fit_transform(headlines).toarray()
        y = dataset['is_sarcastic']

    log.info('Found {} entries'.format(x.shape[0]))
    log.info('Using {} features (words) and a test size of {}'.format(args.max_features or 'ALL', args.test_size))

    model_runner = ClassificationModelRunner(log)
    model_runner.load_single_dataset(x, y, test_size=args.test_size)
    model_runner.run_all()

    elapsed_time = time.time() - start_time
    log.info('The job took {:.4f} seconds to run'.format(elapsed_time))
    log.info('All done!')


if __name__ == "__main__":
    main()
