# Regression Experiments

## Wine Quality
For this experiment I'm going to use this dataset:
https://www.kaggle.com/uciml/red-wine-quality-cortez-et-al-2009

#### Usage
wine_quality [--data|-d] <data file> [--test-size|-t] <float>

where:

* **data**: The location of the CSV input data file
* **test-size**: The percentage of data to use as a test set (between 0 and 1) 

#### Data PreProcessing
The data in this dataset is pretty clean. The only extra operation needed was the normalization of dependent variables. 

In total, the dataset contains 1599 entries each with 11 features + the quality
The dataset was split into a training and a test set with a ratio of 80/20

#### Results
| Model | R2 Score | Time (s) |
|---|---|---|
|**Linear Regression**|0.39|0.002|
|**RBF SVM**|0.44|0.128|
|**Decision Tree**|0.056|0.008|
|**Random Forest**|0.428|0.050|

