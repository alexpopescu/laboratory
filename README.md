# Experiments

This is a repository containing various AI experiments

### Technologies used

Python, NumPy, SciPy, Pandas, SciKit Learn, TensorFlow, Keras

### More information

Check the separate README files:

#### [Clustering](clustering)

2 Algorithms:

* K-Means Clustering
* Hierarchical Clustering

2 Problems:

* Iris classification
* Customer segmentation
 
#### [Movie Recommender](movie_recommender)

A recommender system based on the MovieLens dataset

#### [Regression](regression)

4 Algorithms:

* Linear Regression
* RBF SVM
* Decision Tree
* Random Forest

1 Problem:

* Wine quality

#### [Text Classification](text_classification)

7 Algorithms

* Logistic Regression
* K-Nearest Neighbors
* Linear SVM
* RBF SVM
* Naive Bayes
* Decision Tree
* Random Forest

3 Problems:

* Sarcasm detection in article titles
* Spam classification
* Sentiment analysis of movie reviews

#### [Deep Learning](deep_learning)

1 Algorithm

* Convolutional Neural Networks

2 Problems

* Image classification
* Pneumonia Diagnose using X-Ray images
