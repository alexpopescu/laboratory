import logging
import os
import time
from datetime import datetime

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


def create_logger(log_path=None, log_level=None):
    """
        Creates a logger for the current script
    :return:
    """
    script = '.'.join(os.path.basename(__file__).split('.')[:-1])
    log_level = log_level or logging.DEBUG

    logger = logging.getLogger(script.upper())
    logger.setLevel(log_level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    ch = logging.StreamHandler()
    ch.setLevel(log_level)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    if log_path is not None:
        log_file = os.path.join(log_path, '{}-{}.log'.format(script, datetime.today()))
        fh = logging.FileHandler(log_file)
        fh.setLevel(log_level)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    return logger


class Timer:
    def __init__(self, name, callback=None):
        self.name = name
        self.callback = callback or self.__callback
        self.start_time = 0

    @staticmethod
    def __callback(text):
        print(text)

    def __enter__(self):
        self.callback('Running {}'.format(self.name))
        self.start_time = time.time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        duration = time.time() - self.start_time
        self.callback('{} took {:.8f} second(s)'.format(self.name, duration))


class BaseModelRunner:
    def __init__(self, log):
        self.log = log
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None

    def load_single_dataset(self, documents, labels, test_size=0.2):
        self.x_train, self.x_test, self.y_train, self.y_test = \
            train_test_split(documents, labels, test_size=test_size, random_state=0, stratify=labels)

    def load_train_test_dataset(self, train_documents, train_labels, test_documents, test_labels):
        self.x_train = train_documents
        self.x_test = test_documents
        self.y_train = train_labels
        self.y_test = test_labels

    def _log(self, text):
        self.log.info(text)

    def normalize_data(self):
        scale = StandardScaler()
        self.x_train = scale.fit_transform(self.x_train)
        self.x_test = scale.transform(self.x_test)
