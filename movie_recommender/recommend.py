import argparse
import csv
import os
import statistics
import time
from collections import defaultdict

from movie_recommender import common


FILE_ENCODING = 'utf-8'


def parse_arguments():
    parser = argparse.ArgumentParser(description='Processes movie rating data and makes recommendations')
    parser.add_argument('--data', '-d', dest='data', help='The location of the input data files')
    parser.add_argument('--user', '-u', dest='user', help='The ID of the user to give recommendations to')
    parser.add_argument('--min-movie-ratings', '-r', dest='min_movie_ratings', type=int, default=100,
                        help='The minimum amount of ratings needed for a movie to be taken into account')
    parser.add_argument('--max-user-ratings', '-x', dest='max_user_ratings', type=int, default=1000,
                        help='The maximum amount of ratings needed for a user to be taken into account')
    parser.add_argument('--num-recommendations', '-n', dest='num_recommendations', type=int, default=20,
                        help='The number of movie recommendations to return')
    return parser.parse_args()


def read_dataset(movie_file, rating_file, movies_to_ignore, users_to_ignore, log):
    log.info('Loading movies from %s', movie_file)
    movie_ratings = {}

    # headers: movieId,title,genres
    with open(movie_file, newline='', encoding=FILE_ENCODING) as csv_file:
        movie_reader = csv.reader(csv_file)
        _ = next(movie_reader)
        for row in movie_reader:
            if row[0] in movies_to_ignore:
                continue
            movie_ratings[row[0]] = {
                'title': row[1],
                'genres': row[2].split('|'),
                'ratings': {},
                'users': set()
            }

    log.info('Loading ratings from %s', rating_file)
    user_ratings = defaultdict(dict)

    # headers: userId,movieId,rating,timestamp
    with open(rating_file, newline='') as csv_file:
        rating_reader = csv.reader(csv_file)
        _ = next(rating_reader)
        for row in rating_reader:
            if row[0] in users_to_ignore or row[1] in movies_to_ignore:
                continue
            rating = float(row[2])
            user_ratings[row[0]][row[1]] = rating
            movie_ratings[row[1]]['ratings'][row[0]] = rating
            movie_ratings[row[1]]['users'].add(row[0])

    return movie_ratings, user_ratings


def compute_data_filter(rating_file, given_user_id, max_user_ratings, min_movie_ratings, log):
    given_user_movies = set()
    user_rating_counts = defaultdict(int)
    movie_rating_counts = defaultdict(int)

    # headers: userId,movieId,rating,timestamp
    with open(rating_file, newline='') as csv_file:
        rating_reader = csv.reader(csv_file)
        _ = next(rating_reader)
        for row in rating_reader:
            user_rating_counts[row[0]] += 1
            movie_rating_counts[row[1]] += 1
            if row[0] == given_user_id:
                given_user_movies.add(row[1])

    ratings_per_user = user_rating_counts.values()
    log.info('The dataset contains: %d movies, %d users and %d ratings',
             len(movie_rating_counts), len(user_rating_counts), sum(ratings_per_user))
    log.info('Total user ratings: min=%d, max=%d, mean=%f, stdev=%f',
             min(ratings_per_user), max(ratings_per_user),
             statistics.mean(ratings_per_user), statistics.stdev(ratings_per_user))

    movies_to_ignore = {m for m, c in movie_rating_counts.items() if c < min_movie_ratings}
    movies_to_ignore.difference_update(given_user_movies)
    users_to_ignore = {u for u, c in user_rating_counts.items() if c > max_user_ratings}

    log.info('Removing %d movies because they have less than %d ratings',
             len(movies_to_ignore), min_movie_ratings)
    log.info('Removing %d users because they gave more than %d ratings',
             len(users_to_ignore), max_user_ratings)

    return movies_to_ignore, users_to_ignore


def compute_pearson_correlation(given_user_ratings, movie_ratings):
    movie_correlation = defaultdict(dict)
    for movie_id in given_user_ratings.keys():
        current_movie = movie_ratings[movie_id]
        for other_movie_id, other_movie_data in movie_ratings.items():
            common_users = current_movie['users'].intersection(other_movie_data['users'])
            if len(common_users) <= 1:
                continue
            x = {user_id: rating for user_id, rating in current_movie['ratings'].items() if user_id in common_users}
            xv = list(x.values())
            mx = statistics.mean(xv)
            sx = statistics.stdev(xv)
            y = {user_id: rating for user_id, rating in other_movie_data['ratings'].items() if user_id in common_users}
            yv = list(y.values())
            my = statistics.mean(yv)
            sy = statistics.stdev(yv)
            if sx == 0 or sy == 0:
                continue
            movie_correlation[movie_id][other_movie_id] = \
                sum((x[i] - mx) * (y[i] - my) for i in common_users) / (sx * sy)
    return movie_correlation


def extract_recommendations(given_user_ratings, movie_correlation, num_recommendations):
    similar_movies = []
    for movie_id, rating in given_user_ratings.items():
        for other_movie_id, correlation in movie_correlation[movie_id].items():
            similar_movies.append((correlation * rating, other_movie_id))
    similar_movies.sort(reverse=True)

    added = set()
    recommendations = []
    for score, movie_id in similar_movies:
        # skip movies already rated by the user
        if movie_id in given_user_ratings or movie_id in added:
            continue

        added.add(movie_id)
        recommendations.append((movie_id, score))
        if len(recommendations) >= num_recommendations:
            break
    return recommendations


def main():
    args = parse_arguments()
    log = common.create_logger()
    start_time = time.time()

    log.info('Loading other_movie_data from %s', args.data)

    rating_file = os.path.join(args.data, 'ratings.csv')
    movie_file = os.path.join(args.data, 'movies.csv')

    given_user_id = args.user
    log.info('Checking movies and users to remove')
    movies_to_ignore, users_to_ignore = compute_data_filter(
        rating_file, given_user_id, args.max_user_ratings, args.min_movie_ratings, log)

    movie_ratings, user_ratings = read_dataset(movie_file, rating_file, movies_to_ignore, users_to_ignore, log)

    # compute some statistics
    ratings_per_user = [len(x) for x in user_ratings.values()]
    log.info('Loaded %d movies, %d users and %d ratings',
             len(movie_ratings), len(user_ratings), sum(ratings_per_user))
    log.info('Loaded user ratings: min=%d, max=%d, mean=%f, stdev=%f',
             min(ratings_per_user), max(ratings_per_user),
             statistics.mean(ratings_per_user), statistics.stdev(ratings_per_user))

    given_user_ratings = user_ratings[given_user_id]
    log.debug('Rated movies for user %s', given_user_id)
    for movie_id, rating in given_user_ratings.items():
        log.debug('%s - %f', movie_ratings[movie_id]['title'], rating)

    log.info('Calculating correlation using the Pearson coefficient')
    movie_correlation = compute_pearson_correlation(given_user_ratings, movie_ratings)

    log.info('Building a list of recommendations')
    recommendations = extract_recommendations(given_user_ratings, movie_correlation, args.num_recommendations)

    log.info('Recommended movies:')
    for movie_id, score in recommendations:
        movie_title = movie_ratings[movie_id]['title']
        log.info('%s -> %f', movie_title, score)

    elapsed_time = time.time() - start_time
    log.info('The job took %f seconds to run', elapsed_time)
    log.info('All done!')


if __name__ == "__main__":
    main()
