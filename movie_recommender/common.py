import logging
import os
from datetime import datetime


def create_logger(log_path=None, log_level=None):
    """
        Creates a logger for the current script
    :return:
    """
    script = '.'.join(os.path.basename(__file__).split('.')[:-1])
    log_level = log_level or logging.DEBUG

    logger = logging.getLogger(script.upper())
    logger.setLevel(log_level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    ch = logging.StreamHandler()
    ch.setLevel(log_level)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    if log_path is not None:
        log_file = os.path.join(log_path, '{}-{}.log'.format(script, datetime.today()))
        fh = logging.FileHandler(log_file)
        fh.setLevel(log_level)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    return logger
