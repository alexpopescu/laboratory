import argparse
import io
import os
import time

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer

import common
from text_classification import util
from text_classification.model_runner import ClassificationModelRunner


def parse_arguments():
    parser = argparse.ArgumentParser(description='Classifies messages as SPAM or HAM')
    parser.add_argument('--data', '-d', dest='data',
                        help='The dataset directory (should contain subdirectories "ham" and "spam")')
    parser.add_argument('--max-features', '-m', dest='max_features', default=None, type=int,
                        help='The maximum number of features to consider (top)')
    parser.add_argument('--test-size', '-t', dest='test_size', default=0.2, type=float,
                        help='The percentage of data to be used for testing (given as a number between 0 and 1)')
    return parser.parse_args()


def read_files(path):
    for root, _, filenames in os.walk(path):
        for filename in filenames:
            path = os.path.join(root, filename)

            in_body = False
            lines = []
            f = io.open(path, 'r', encoding='latin1')
            for line in f:
                if in_body:
                    lines.append(line)
                elif line == '\n':
                    in_body = True
            f.close()
            message = '\n'.join(lines)
            yield path, message


def data_frame_from_directory(path, classification):
    rows = []
    index = []
    for filename, message in read_files(path):
        rows.append({'message': message, 'class': classification})
        index.append(filename)

    return pd.DataFrame(rows, index=index)


def main():
    args = parse_arguments()
    log = common.create_logger()
    start_time = time.time()

    with common.Timer('Loading dataset from {}'.format(args.data), callback=log.info):
        dataset = pd.DataFrame({'message': [], 'class': []})

        dataset = dataset.append(data_frame_from_directory(os.path.join(args.data, 'spam'), 'spam'), sort=False)
        dataset = dataset.append(data_frame_from_directory(os.path.join(args.data, 'ham'), 'ham'), sort=False)

    with common.Timer('Data PreProcessing', callback=log.info):
        n = dataset.shape[0]
        messages = [util.text_clean_up(dataset['message'][i]) for i in range(0, n)]

        cv = CountVectorizer(max_features=args.max_features)
        x = cv.fit_transform(messages).toarray()
        y = dataset['class']

    log.info('Found {} entries'.format(x.shape[0]))
    log.info('Using {} features (words) and a test size of {}'.format(args.max_features or 'ALL', args.test_size))

    model_runner = ClassificationModelRunner(log)
    model_runner.load_single_dataset(x, y, test_size=args.test_size)
    model_runner.run_all()

    elapsed_time = time.time() - start_time
    log.info('The job took {:.4f} seconds to run'.format(elapsed_time))
    log.info('All done!')


if __name__ == "__main__":
    main()
