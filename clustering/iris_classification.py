import argparse
import time

import pandas as pd

import common
from clustering.model_runner import ClusteringModelRunner


def parse_arguments():
    parser = argparse.ArgumentParser(description='Classifies Iris flowers by clustering')
    parser.add_argument('--data', '-d', dest='data', help='The location of the JSON input data file')
    return parser.parse_args()


def main():
    args = parse_arguments()
    log = common.create_logger()
    start_time = time.time()

    with common.Timer('Loading dataset from {}'.format(args.data), callback=log.info):
        dataset = pd.read_json(args.data)
        x = dataset.iloc[:, :-1].values
        y = dataset.iloc[:, -1].values
        columns = dataset.columns.tolist()[:-1]

    model_runner = ClusteringModelRunner(log)
    model_runner.load_dataset(x, columns, y)

    with common.Timer('Elbow method', callback=log.info):
        k_elbow = model_runner.use_elbow_method()
        log.info('Found number of clusters to be %d', k_elbow)

    with common.Timer('Dendrogram', callback=log.info):
        k_dendrogram = model_runner.use_dendrogram()
        log.info('Found number of clusters to be %d', k_dendrogram)

    # take the max between the two cluster count values found
    model_runner.k = max(k_elbow, k_dendrogram)
    model_runner.run_all()

    elapsed_time = time.time() - start_time
    log.info('The job took {:.4f} seconds to run'.format(elapsed_time))
    log.info('All done!')


if __name__ == "__main__":
    main()
