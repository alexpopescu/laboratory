import argparse
import logging
import math
import os
import time

import pandas as pd

from keras import optimizers
from keras.layers import Convolution2D, MaxPooling2D, Flatten, Dense, Dropout
from keras.models import Sequential
from keras_preprocessing.image import ImageDataGenerator
from pandas import DataFrame
from sklearn.model_selection import train_test_split

import common
from deep_learning.util import create_keras_callback


def parse_arguments():
    parser = argparse.ArgumentParser(description='Learns pneumonia diagnosis from lung X-rays')
    parser.add_argument('--data', '-d', dest='data', help='The location of the image dataset')
    parser.add_argument('--epochs', '-e', dest='epochs', default=25, type=int,
                        help='The number of epochs to use for the training')
    return parser.parse_args()


def read_dataset(data_path):
    train_data = []
    test_data = []
    train_path = os.path.join(data_path, 'train')
    test_path = os.path.join(data_path, 'test')
    classes = ['NORMAL', 'PNEUMONIA']

    for class_name in classes:
        class_train_path = os.path.join(train_path, class_name)
        _, _, files = next(os.walk(class_train_path))
        for img_file in files:
            train_data.append({'filename': os.path.join(class_train_path, img_file), 'class': class_name})

        class_test_path = os.path.join(test_path, class_name)
        _, _, files = next(os.walk(class_test_path))
        for img_file in files:
            test_data.append({'filename': os.path.join(class_test_path, img_file), 'class': class_name})

    train_dataset = DataFrame(train_data, columns=['filename', 'class'])
    test_dataset = DataFrame(test_data, columns=['filename', 'class'])
    return train_dataset, test_dataset


def main():
    args = parse_arguments()
    log = common.create_logger(log_level=logging.INFO)
    start_time = time.time()

    with common.Timer('Reading dataset', callback=log.info):
        train, test = read_dataset(args.data)

    # generates variations of images to augment the data set (shear, zoom, flip)
    # this is to prevent overfitting
    train_datagen = ImageDataGenerator(rescale=1./255,
                                       shear_range=0.2,
                                       zoom_range=0.2,
                                       horizontal_flip=True)
    test_datagen = ImageDataGenerator(rescale=1./255)

    # create the image training set and test set
    training_set = train_datagen.flow_from_dataframe(
        dataframe=train,
        target_size=(64, 64),
        batch_size=32,
        class_mode='binary'
    )

    test_set = test_datagen.flow_from_dataframe(
        dataframe=test,
        target_size=(64, 64),
        batch_size=32,
        class_mode='binary'
    )

    # create the CNN
    classifier = Sequential()

    # Step 1 - Convolution
    # for input_shape the order of arguments is different when using a TensorFlow backend
    classifier.add(Convolution2D(filters=32,
                                 kernel_size=(3, 3),
                                 input_shape=(64, 64, 3),
                                 activation='relu'))

    # Step 2 - Max Pooling
    classifier.add(MaxPooling2D(pool_size=(2, 2)))

    # adding a second convolutional layer to improve performance
    classifier.add(Convolution2D(filters=32, kernel_size=(3, 3), activation='relu'))
    classifier.add(MaxPooling2D(pool_size=(2, 2)))

    # Step 3 - Flattening
    classifier.add(Flatten())

    # Step 4 - Full Connection
    # one hidden layer and one output layer
    classifier.add(Dense(units=128, activation='relu'))
    classifier.add(Dropout(0.2))
    classifier.add(Dense(units=128, activation='relu'))
    classifier.add(Dropout(0.2))
    classifier.add(Dense(units=128, activation='relu'))
    classifier.add(Dropout(0.2))
    classifier.add(Dense(units=128, activation='relu'))
    classifier.add(Dropout(0.2))
    classifier.add(Dense(units=1, activation='sigmoid'))

    # compiling the CNN
    optimizer = optimizers.SGD(lr=0.01)
    classifier.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'])

    # fits the model to the training set and evaluates against the test set
    classifier.fit_generator(generator=training_set,
                             steps_per_epoch=math.ceil(training_set.n/32),
                             epochs=args.epochs,
                             validation_data=test_set,
                             validation_steps=math.ceil(test_set.n/32),
                             callbacks=[create_keras_callback(log)],
                             verbose=0)

    elapsed_time = time.time() - start_time
    log.info('The job took {:.4f} seconds to run'.format(elapsed_time))
    log.info('All done!')


if __name__ == "__main__":
    main()
